package com.day2test;

class Room{
	int roomNo;
	String roomType;
	double roomArea;
	boolean acMachine;
	void setData(int rn , String i , double ra , boolean acm) {
		 roomNo = rn;
		 roomType = i;
		 roomArea = ra;
		 acMachine = acm;
	}
	void displayData() {
		System.out.println("The room no is "+roomNo);
		System.out.println("The room type is "+roomType);
		System.out.println("The room Area is "+roomArea);
		String s = (acMachine)?"needed" : "not needed";
		System.out.println("AC machine is " +s);
	}
	
}
public class RoomDetails {

	public static void main(String[] args) {
		Room a = new Room();
		a.setData(5,"Premium",440,false);
		a.displayData();

	}

}
