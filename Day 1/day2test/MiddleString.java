package com.day2test;

import java.util.Scanner;

class Test{
	public static String middleCharacter(String text){
		if (text.length()%2==0){
			double midVal=text.length()/2;
			int x1=(int)midVal;
			int x2=(int)x1-1;
			String mid1=Character.toString(text.charAt(x1));
			String mid2=Character.toString(text.charAt(x2));
			String midFinal=mid2+mid1;
			return midFinal;
		}
		else{
			int a=text.length()/2;
			String s=Character.toString(text.charAt(a));
			return s;
		}
	}
}
public class MiddleString {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String text=sc.nextLine();
		System.out.println("The middle character is "+Test.middleCharacter(text));
		sc.close();
	}

}
