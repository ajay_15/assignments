package com.day2test;

class A{
	void method1() {
		System.out.println("prnt 1" );
	}
}
	class B extends  A{
		void method() {
			System.out.println("Print 2");
		}
}
public class Inherit {

	public static void main(String[] args) {
		B m = new B();
		m.method();
		A x = new A();
		x.method1();

	}

}
