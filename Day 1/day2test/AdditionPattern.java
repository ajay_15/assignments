package com.day2test;

import java.util.Scanner;

public class AdditionPattern {
         public void addition(int[] arr){
            int sum=0;
            String s="";
            for(int i=0;i<arr.length;i++){
                if(i>0){
                    s= s + "+";
                }
             s= s + arr[i];
                sum = sum + arr[i];
           
                System.out.println(s + "=" + sum);;
            }
         }
        
           public static void main(String []args)
           {
               Scanner sc = new Scanner(System.in);
               System.out.println("Enter the number count:");
               int count = sc.nextInt();
               int[] numberArr = new int[count];
               for(int i=0;i<count;i++){
                   System.out.println("Enter the number input" + (i+1) + ":");
                    numberArr[i] = sc.nextInt();
               }
               AdditionPattern a= new AdditionPattern();
               a.addition(numberArr);
               sc.close();
        }   
}
