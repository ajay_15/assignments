package com.day1test;

import java.util.Scanner;

class Dig{
	public static int checkSum(int num) {
		if(num > 0) {
		int sum=0;
	    while(num>0){
	      int rem = num%10;
	      if(rem%2!=0){
	        sum = sum+rem;
	      }
	      num = num/10;
	    }
	    
	    if(sum%2!=0){
	      System.out.println("Sum of odd digits is odd");
	    }else if(sum%2==0){
	      System.out.println("Sum of odd digits is even");
	    }
		}
		return -1;
	}
}
public class SumOfOdd {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
	    int n = input.nextInt();
	    input.close();
	    Dig.checkSum(n);

	}

}
