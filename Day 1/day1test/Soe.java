package com.day1test;

import java.util.Scanner;

class UserMainCode {
	public static int sumOfSquaresOfEvenDigits(int n){
		int rem;
		int sum=0;
		if (n>0){
			while (n>0){
				rem=n%10;
				n=n/10;
				if (rem%2==0){
					sum=sum+(rem*rem);
				}
			}
			if(sum==0){
				return 1;
			}
			else{
				return sum;
			}
		}
		else {
			return 0;
		}
	}
}
public class Soe {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int input=sc.nextInt();
		int a=UserMainCode.sumOfSquaresOfEvenDigits(input);
		if (a==0){
			System.out.println("Not a Positive Number");
		}
		else if(a==1){
			System.out.println("Not an even number");
		}
		else{
			System.out.println("Sum of square of the even digits is "+a);
			sc.close();
		}
	}

}
