package com.day1test;

public class Swap {

	public static void main(String[] args) {
		int a, b, temp;
		   a = 5;
		   b = 7;
		   temp = a;
		   a = b;
		   b = temp;   
		  System.out.println("After swapping :" +"a =" +a +" and " +"b " +b);

	}

}
