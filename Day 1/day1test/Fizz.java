package com.day1test;

public class Fizz {

	public static void main(String[] args) {
		 for (int i = 1; i <= 100; i++) {
	            if (i % 3 == 0 && i % 5 == 0) {
	                System.out.println("Number :" +i +"fizz buzz");
	            } else if (i % 5 == 0) {
	            	System.out.println("Number :" +i +" buzz");
	            } else if (i % 3 == 0) {
	            	System.out.println("Number :" +i +" fizz");
	            } 
	        }
	    }
}

