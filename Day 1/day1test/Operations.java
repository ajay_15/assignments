package com.day1test;

class Exe{
	int sum(int a , int b) {
		return a+b;
	}
	int sub(int a, int b) {
		return a-b;
	}
	int mul(int a, int b) {
		return a*b;
	}
	double div(double a , double b) {
		return a/b;
	}
	double rem(double a,double b) {
		return a%b;
	}
}
public class Operations {

	public static void main(String[] args) {
		Exe ob = new Exe();
		System.out.println("Sum is :" +ob.sum(20, 4));
		System.out.println("Sub is :" +ob.sub(20, 4));
		System.out.println("Mul is :" +ob.mul(20, 4));
		System.out.println("Div is :" +ob.div(20, 4));
		System.out.println("Rem is :" +ob.rem(20, 4));
	
	}

}
