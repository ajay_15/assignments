package com.day5test;

import java.util.*;
public class TryCatch {
	 public static void main(String[] args) {
	        Scanner sc = new Scanner(System.in);
	        try{
	        try{
	            int input1 = new Integer(sc.nextInt());
	            int input2 = new Integer(sc.nextInt());
	            System.out.println(""+(input1/input2));
	        }catch (InputMismatchException e){
	            System.out.println("java.util.InputMismatchException");
	        }
	        }catch(Exception e){
	            System.out.println(e);
	        }
	        sc.close();
	        
	    }
}
