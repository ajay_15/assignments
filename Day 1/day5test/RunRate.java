package com.day5test;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GetRR{
	GetRR(int totalruns,int overs){
		try {
			float Runrate = totalruns/overs;
			System.out.println("Current Run rate :" +Runrate);
		}catch (Exception e) {
			System.out.println("java.lang.ArithmeticException");
		}
	}
}
public class RunRate {
	public static void main(String[] args) throws NumberFormatException,IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the total runs ");
		Integer totalruns = Integer.parseInt(reader.readLine());
		System.out.println("Enter total overs ");
		Integer overs = Integer.parseInt(reader.readLine());
		GetRR rr = new GetRR(totalruns,overs);
	}

}
