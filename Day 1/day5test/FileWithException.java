package com.day5test;

import java.io.*;
public class FileWithException {

	public static void main(String[] args) {
		String filename = "C:\\DummyText.txt";
		File file = new File(filename);
		try {
			FileReader fileReader = new FileReader(file);
		}
		catch(FileNotFoundException e) {
			System.out.println(e);
		}

	}

}
