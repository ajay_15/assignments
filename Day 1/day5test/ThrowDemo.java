package com.day5test;

import java.util.Scanner;

class myCalculator{
    int power(int number1, int number2) throws Exception{
        if(number1<=0 || number2<=0)
            throw new Exception("number1 and number2 should be non-negative");
        return (int)Math.pow((double)number1,(double)number2);
    }
}
public class ThrowDemo {
	public static void main(String []argh)
    {
        Scanner sc = new Scanner(System.in);

        while(sc.hasNextInt())
        {
            int a = sc.nextInt();
            int b = sc.nextInt();
            myCalculator C = new myCalculator();
            try
            {
                System.out.println(C.power(a,b));
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
        sc.close();

    }
}
