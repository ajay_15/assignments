package com.day5test;

import java.util.Scanner;

class CustomExcep extends Exception{
	public CustomExcep(String msg) {
		super(msg);
	}
}
public class NameAge {

	public static void main(String[] args) {
		Scanner c = new Scanner(System.in);
		System.out.println("Enter the player name");
		String name =c.nextLine();
		System.out.println("Enter the player age");
		int age = c.nextInt();
		try {
			if(age<19)
				throw new CustomExcep("CustomException : InvalidAge");
			else
				System.out.println("Player name :" +name);
			    System.out.println("Player age :" +age);
		}
		catch(CustomExcep e) {
			System.out.println(e.getMessage());
		}

	}

}
