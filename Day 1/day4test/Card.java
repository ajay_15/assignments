package com.day4test;

public abstract class Card {

private String holderName;
private int cardNumber;
private int expiryDate;

public Card(String holderName, int cardNumber, int expiryDate) {
super();
this.holderName = holderName;
this.cardNumber = cardNumber;
this.expiryDate = expiryDate;
}

public String getHolderName() {
return holderName;
}

public void setHolderName(String holderName) {
this.holderName = holderName;
}

public int getCardNumber() {
return cardNumber;
}

public void setCardNumber(int cardNumber) {
this.cardNumber = cardNumber;
}

public int getExpiryDate() {
return expiryDate;
}

public void setExpiryDate(int expiryDate) {
this.expiryDate = expiryDate;
}

}


