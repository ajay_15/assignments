package com.day4test;

import java.util.Scanner;

public class Main1Day4 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the shape name");
		System.out.println("1.Circle\n2.Rectangle\n3.Square");
		String ch = s.next();
		if(ch.equals("Circle")) {
			System.out.println("Enter the radius");
			int r=s.nextInt();
			Circle obj1 = new Circle("Circl",r);
			System.out.printf("%.2f",obj1.calculateArea());
		}
		else if(ch.equals("Rectangle")) {
			System.out.println("Enter the length");
			int l=s.nextInt();
			System.out.println("Enter the width");
			int w=s.nextInt();
			Rectangle obj2 = new Rectangle("Rect",l,w);
			System.out.printf("%.2f",obj2.calculateArea());
		}
		else if(ch.equals("Square")) {
			System.out.println("Enter the side");
			int sid=s.nextInt();
			Circle obj3 = new Circle("Sqr",sid);
			System.out.printf("%.2f",obj3.calculateArea());
		}
		s.close();
		
	}

}
