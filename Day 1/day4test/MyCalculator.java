package com.day4test;

import java.util.Scanner;

interface AdvancedArithmetic{
	void print();
}
public class MyCalculator implements AdvancedArithmetic {
	public void print() {
		System.out.println("Implements AdvancedArithmetic\n");
	}
	public int divisor_sum(int n) {
		int sum =0;
		for(int i=1;i<=n;i++) {
			if(n%i==0) {
				sum=sum+i;
			}
		}
		return sum;
	}

	public static void main(String[] args) {
		 MyCalculator objj = new  MyCalculator();
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Enter a number");
		 int a = sc.nextInt();
		 objj.print();
		 System.out.println(objj.divisor_sum(a));
		 sc.close();
	}

}
