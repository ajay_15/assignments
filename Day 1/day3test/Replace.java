package com.day3test;

public class Replace {

	public static void main(String[] args) {
		String str = "shepard";
        String changedStr = str.replace('d' , 'h');
        System.out.println("Original String: " + str);
        System.out.println("String in lowercase: " + changedStr);

	}

}
