package com.day3test;

import java.util.Scanner;

class UserMainCode2{
	public static String reshape(char str,String sep)
	{
		String rev="";
		char[] charr = sep.toCharArray();
	    for (int i = charr.length - 1 ; i >= 0 ; i--) 
	     if(i==0) {
	    rev+=charr[0];
	     }
	     else {
	    	 rev+=charr[i] + Character.toString(str);
	     }
	    return rev;
	}
		
}
public class Reverse {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string");
		String s=sc.nextLine();
		System.out.println("Enter the seperator");
		char charrr = sc.nextLine().charAt(0);
		System.out.println("The Final String "+UserMainCode2.reshape(charrr,s));
		sc.close();

	}

}
