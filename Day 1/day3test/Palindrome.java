package com.day3test;

import java.util.Scanner;

class Check{
	static boolean isPalindrome(String str)
    {
        int i = 0, j = str.length() - 1;
        while (i < j) {
            if (str.charAt(i) != str.charAt(j))
                return false;
            i++;
            j--;
        }
        return true;
    }
}
public class Palindrome {

	public static void main(String[] args) {
		
		 Scanner sc = new Scanner(System.in);
         System.out.println("Enter the string:");
         String s = sc.nextLine();
         sc.close();
        if(Check.isPalindrome(s)) {
			System.out.print("Yes");
		}
		else {
			System.out.print("No");
		}   
	}

}
