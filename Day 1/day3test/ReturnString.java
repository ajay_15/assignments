package com.day3test;

import java.util.Scanner;

class UserMainCode{
	public static String getString(String stng) 
	{   
	  int len = stng.length();
	  String temp = "";
	  for (int i = 0; i < len; i++) 
	  {
	    if (i == 0 && stng.charAt(i) == 'k')
	      temp += 'k';
	    else if (i == 1 && stng.charAt(i) == 'b')
	      temp += 'b';
	    else if (i > 1)
	      temp += stng.charAt(i);
	  }
	    return temp;
	}
}
public class ReturnString {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String s=sc.nextLine();
		System.out.println("The Final String "+UserMainCode.getString(s));
		sc.close();
		
	}

}

