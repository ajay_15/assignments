package com.day3test;

public class LowerString {

	public static void main(String[] args) {
		String str = "HCL Technologies";
        String lowerStr = str.toLowerCase();
        System.out.println("Original String: " + str);
        System.out.println("String in lowercase: " + lowerStr);

	}

}
