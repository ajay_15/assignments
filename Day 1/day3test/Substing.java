package com.day3test;

import java.util.Scanner;

public class Substing {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String :");
        String str = sc.next();
        System.out.println("Enter the start range :");
        int start  = sc.nextInt();
        System.out.println("Enter the End range :");
        int end    = sc.nextInt();
        sc.close();
        System.out.println(str.substring(start, end));
	}

}
